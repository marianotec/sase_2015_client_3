/**
 * DOXYGEN COMMENTS
 *
 * @file   c_main.c
 *
 * @Author Koremblum, Nicolás Mariano (marianotec7@gmail.com)
 * 		   Di Donato, Andrés (adidonato@gmail.com)
 *
 * @date   31/7/2015 [DD/MM/YYYY]
 *
 * @brief  Cliente, ejemplo #3, SASE 2015, Programación sobre Linux (2/3)
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

/* **************************************************************** */
/* 								INCLUDEs							*/

#include "c_main.h"

/* **************************************************************** */
/* 								DEFINEs								*/

/* **************************************************************** */
/* 								MACROs								*/

#define MAX(x,y)			((x>y)?x:y)

#define	ENDLINE2NULL		for(index=0;messageBuffer[index]!='\n'&&messageBuffer[index]!='\0';index++);messageBuffer[index]='\0';index=0

#define	IS_KEYBOARD			(FD_ISSET(KEYBOARD_FD,&fdsAuxiliar))
#define IS_TCP_SOCK			(FD_ISSET(tcpSocketFD,&fdsAuxiliar))
#define IS_UDP_SOCK			(FD_ISSET(udpSocketFD,&fdsAuxiliar))

#define CLEAR_SCREEN		system("clear")

#define	IS_EXIT_MESSAGE		(0==strcmp("exit",messageBuffer))

/* **************************************************************** */
/* 								GLOBALs								*/

// Add global variable's here

/* **************************************************************** */
/* 								 CODE								*/

int main (int argc, char *argv[])
{
	/** ************    VARIABLE's DECLARATION    ************ **/

	int new = 0;								// Variable utilizada para la visualización de la información recibida

	int prev = 0;								// Variable utilizada para la visualización de la información recibida

	int index = 0;								// Variable utilizada para recorrer vectores y como auxiliar en lazos

	int exitVar = FALSE;						// Variable que indica si se desea salir del loop principal o no

	int returnValue = -1;						// Variable que toma el valor que retornan las funciones

	int selectValue = -1;						// Variable que toma el valor que retornan la funcion select

	int fixedMaxFD = 0;							// Variable que utiliza el handler del select

	int tcpSocketFD = -1;						// Variable que contiene el file descriptor del socket TCP

	int udpSocketFD = -1;						// Variable que contiene el file descriptor del socket TCP

	unsigned int addr_len = 0;					// Variable que contiene el tamaño de la estructura sokaddr

	struct timeval timeout;						// Estructura para setear el timeout del Select

	fd_set fdsMaster;							// Variable que utiliza el handler del Select

	fd_set fdsAuxiliar;							// Variable auxiliar para reconfigurar el handler del Select

	char messageBuffer[MAX_MESSAGE];			// Vector donde se guardan los mensajes recibidos

	struct sockaddr_in tcp_serverAddress;		// Estructura que contiene información del servidor TCP

	struct sockaddr_in udp_serverAddress;		// Estructura que contiene información del servidor UDP

	/** ****************************************************** **/

	check_arg(argc);

	CLEAR_SCREEN;

	signal(SIGINT,ctrlC_signalHandler);

	printf("\nDebug(Cliente): Conectandose al servidor TCP...\n");

	//Me conecto al servidor TCP
	tcpSocketFD = tcp_connect(argv[1],atoi(argv[2]),&tcp_serverAddress);

	//Verifico que no haya errores al abrir el socket TCP
	if(IS_ERROR(tcpSocketFD))
	{
		perror("Error al abrir el socket TCP\n");

		return -1;
	}

	printf("\nDebug(Cliente): Conexión con el servidor TCP establecida.\n");
	printf("\nDebug(Cliente): Conectandose al servidor UDP...\n");

	//Me conecto al servidor UDP
	udpSocketFD = udp_connectionClient(argv[1],(atoi(argv[2])+1),&udp_serverAddress);

	//Verifico que no haya errores al abrir el socket UDP
	if(IS_ERROR(udpSocketFD))
	{
		perror("Error al abrir el socket UDP\n");

		close(tcpSocketFD);

		return -1;
	}

	printf("\nDebug(Cliente): Conexión con el servidor UDP establecida.\n");

	/** ************    Seteos de la función Select    ************ **/

	//Inicializo el file descriptor handler
	FD_ZERO( &fdsMaster );

	//File Descriptos a monitorear
	FD_SET( KEYBOARD_FD, &fdsMaster ); 	//Seteo el teclado
	FD_SET( tcpSocketFD, &fdsMaster ); 		//Seteo el socket TCP
	FD_SET( udpSocketFD, &fdsMaster ); 		//Seteo el socket TCP

	//Guardo en esta variable el valor del file descriptor más grande y le sumo uno. (Para más información: man select)
	fixedMaxFD = MAX(tcpSocketFD,udpSocketFD) + 1;

	//Seteos timeout
	timeout.tv_sec 	= SEC_SELECT_TIMEOUT;
	timeout.tv_usec = USEC_SELECT_TIMEOUT;

	/** ****************************************************** **/

	/**		MAIN LOOP		**/
	while(FALSE == exitVar)
	{
		bzero(messageBuffer,MAX_MESSAGE);		//Limpio el buffer de mensajes

		fdsAuxiliar = fdsMaster; //Copio en Auxiliar el Master, dado que el mismo se "reinicia" en cada loop

		if( (selectValue = select( fixedMaxFD , &fdsAuxiliar , NULL , NULL , &timeout )) >= 0)
		{
			/**
			 * SELECT STATEMENT: "Interrupción" por teclado
			 **/
			if( IS_KEYBOARD )
			{
				//Capturo el comando ingresado por teclado
				fgets(messageBuffer, MAX_MESSAGE, stdin);

				//Reemplazo el fin de linea ('\n') por un '\0'
				ENDLINE2NULL;

				if( IS_EXIT_MESSAGE )	exitFunction(&exitVar,tcpSocketFD,"exit");
			}

			/**
			 * SELECT STATEMENT: "Interrupción" por socket TCP
			 **/
			else if( IS_TCP_SOCK )
			{
				read(tcpSocketFD, messageBuffer, MAX_MESSAGE);

				if( IS_EXIT_MESSAGE )
				{
					printf("\nDebug(Cliente): Se recibió el comando de escape por el socket...\n");

					exitVar = TRUE;
				}
			}

			/**
			 * SELECT STATEMENT: "Interrupción" por socket UDP
			 **/
			else if( IS_UDP_SOCK )
			{
				addr_len = sizeof(struct sockaddr);

				returnValue = recvfrom(udpSocketFD,messageBuffer,MAX_MESSAGE,0,(struct sockaddr *)&udp_serverAddress,&addr_len);

				if(0 < returnValue)
				{
					new = atoi(messageBuffer);

					if(new!=prev)
					{
						if( new != (prev+1) )
							printf("> > > >");

						printf("%d\n",new);

						prev = new;
					}
				}
			}

			else if( IS_TIMEOUT( selectValue ) ) //timeout
			{
				sendto(udpSocketFD,"get",strlen("get"),0,(struct sockaddr *)&udp_serverAddress,sizeof(udp_serverAddress));

				//Reemplazo el fin de linea ('\n') por un '\0'
				ENDLINE2NULL;

				//Seteos timeout
				timeout.tv_sec 	= SEC_SELECT_TIMEOUT;
				timeout.tv_usec = USEC_SELECT_TIMEOUT;
			}
		}
	}

	printf("\nDebug(Cliente): Cerrando la aplicación Cliente...\n");

	close(tcpSocketFD);

	close(udpSocketFD);

	return 0;
}

//---------------------------------  Funciones Auxiliares ---------------------------------

void exitFunction(int *exitVar, int wFD, char *message)
{
	*exitVar = TRUE;

	write(wFD, message, strlen(message));
}

void ctrlC_signalHandler(int useless) //SIGINT
{
	printf("\n\nEsa no es la forma correcta de cerrar la aplicación, por favor tipee \"exit\"\n\n");
}

void check_arg(int argc)
{
	if(argc != 3)
	{
		printf("\n\nModo de utilización: ./exec IP PORT\n\n");

		exit(1);
	}
}
